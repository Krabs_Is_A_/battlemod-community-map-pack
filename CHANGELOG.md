## Version 13 (thanks to PeneBoi for compiling this update)
MISC CHANGES:
- Updated some music to reduce filesize and fix loop points.

MAP UPDATES:

Community Maps:
- Marina Madness by PeneBoi *NEW MAP*
- Warped Moonlight by InferNOr *NEW MAP*
- Symmetri-Canyon by PapsTheEchidna
- Downpour Valley by happyalm

Edits of STJR Maps:
- Nimbus Ruins by STJR, PeneBoi *NEW MAP*

## Version 12 (thanks to PeneBoi for compiling this update)
MAP UPDATES:

Community Maps:
- Secluded Island by Krabs
- Greenflower 1 (CTF) by STJR, Krabs
- Techno Hill 1 (CTF) by STJR, Krabs
- Lime Forest by STJR, Krabs
- Blazing Precipice by Krabs
- Haunted Heights (CTF) by STJR, Krabs
- Hidden Chamber by Krabs
- Downpour Valley by happyalm
- Nimbus Sanctuary by RadSla (formerly known as Night Sanctuary)
- Jade Valley edited by izzy
- Agate Hill by RadSla *NEW MAP*
- Honey Garden by +bruh+ *NEW MAP*
- Secluded Wood by PeneBoi, STJR *NEW MAP*
- Cooler Nimbus by PeneBoi, DuckInsurance, STJR *NEW MAP*

Edits of STJR Maps:
- Lime Forest edited by STJR, Krabs
- Silver Cascade edited by STJR, Krabs
- Clockwork Towers edited by Krabs, happyalm

## Version 11
MAP UPDATES:

Community Maps:
- Acidic Wildwood *NEW MAP*
- Celestial Sanctuary by happyalm
- Facing Worlds by Krabs, happyalm *NEW MAP*
- Jagged Creek by RoyKirbs

Edits of STJR Maps:
- Lime Forest edited by Krabs

## Version 10
MAP UPDATES:

Community Maps:
- Tree Ring by STJR, Ice Face
- Launch Base by Ice Face
- Hill Top by Ice Face
- Isolated Gorge by Krabs
- Tree Ring by STJR, Ice Face
- Isolated Island by Ice Face *NEW MAP*

Edits of STJR Maps:
- Desolate Twilight by InferNOr *NEW MAP*
- Celestial Sanctuary by happyalm *NEW MAP*
- Iron Turret edited by Ice Face

## Version 9
MAP UPDATES:

Community Maps:
- Conveyor Conflict
- Collision Chaos by Ice Face *NEW MAP*
- Isolated Gorge by Krabs *NEW MAP*
- Battleball Field by Ice Face *NEW MAP*
- Eerie Valley by Ice Face *NEW MAP*
- Crying Tower by Ice Face *NEW MAP*

Edits of STJR Maps:
- Lost Palace edited by happyalm
- Twisted Terminal edited by Krabs
- Jade Valley edited by izzy, happyalm *NEW MAP*

## Version 8 (thanks again to Logan8r for compiling this update)

New maps:
- MAP71 ARK Observatory by Inazuma
- MAP41 Collision Chaos by Ice Face
- MAP58 Mushroom Hill by Ice Face
- MAP52 Spring Yard by Ice Face
- MAP53 Stardust Speedway by Ice Face
- MAP54 Sky Road 3 by Zipper, Logan8r
- MAP65 Twin Hills by STJR, Ice Face

Updated maps:
- Moonlight by Krabs
- Sapphire Falls (CTF) by STJR, Krabs 
- Black Core 2 (Arena) by STJR, PeneBoi
- Symmetri-Canyon by PapsTheEchidna
- Conveyor Conflict by STJR, Ice Face
- Tree Ring by STJR, Ice Face
- Castle Eggman 2 (CP) by STJR, Ice Face
- Garden Arena by Chuckles troll
- Downpour Valley by happyalm
- Orange Crater by STJR, PeneBoi, CyanKnight
- Green Hill by Ice Face
- Factory Rooftops by InferNOr
- Azure Temple (CTF) by STJR, Ice Face
- Greenflower 2 (CTF) by STJR, PeneBoi
- Jagged Creek by RoyKirbs
- CTF Big Arch by Kwiin
 
Rebalanced & edited vanilla maps (these replace the original maps):
- Twisted Terminal edited by Krabs
- Icicle Falls edited by happyalm, InferNOr
- Clockwork Towers edited by Krabs
- Iron Turret edited by Ice Face
- Dual Fortress edited by InferNOr, Chuckles troll
- Meadow Match edited by izzy, DuckInsurance

## Version 7 (big thanks for Logan8r for helping compile this update)
New maps:
- Greenflower 2 (CTF) by STJR, PeneBoi
- Black Core 2 (Arena) by STJR, PeneBoi
- Air Haven by STJR, PeneBoi
- Garden Arena by Chuckles trollface
- Arcanum Woodland by Raze, Logan8r
- Mushroom Hill 
- Bowser in the Sky by Logan8r
- Orange Crater by STJR, PeneBoi, CyanKnight
- Vapor Garden by Jazzz, Chrispy, Logan8r  
- Vine Cavern by STJR, InferNOr, Logan8r
- Isolated Canyon by STJR, PeneBoi
- Downpour Valley by happyalm
- Sapphire Falls (CTF) by STJR, Krabs 
- Azure Temple (Arena) by STJR, Ice Face
- Stupid Facestabber by Krabs
- GFZFLR01 by Krabs

New Bonus Maps (only accessible via console):
- Submerged Temple by STJR, PeneBoi
- Old Iron Turret by STJR, PeneBoi
- Flooded Temple by STJR, PeneBoi
- Zero Ring by STJR, PeneBoi, CyanKnight
- Iron Warehouse by STJR, PeneBoi

Updated maps:
- Secluded Island
- Tempest Stronghold
- Blazing Precipice
- Symmetri-Canyon
- Sunspot Platform
- Stratos Valkyrie

Rebalanced & edited vanilla maps (these replace the original maps):
- Lost Palace edited by happyalm
- Twisted Terminal edited by Krabs
- Lime Forest edited by Krabs
- Silver Cascade edited by Krabs

## Version 6
New maps:
- Castle Eggman 3 (Arena) by STJR, Krabs
- Lost Ark by NeroTH

## Version 5
New maps:
- Green Hill by Ice Face
- Hill Top by Ice Face
- Launch Base 2 by Ice Face

Updated maps:
- Haunted Heights (CTF)
- Symmetri-Canyon
- Stratos Valkyrie

## Version 4
New maps:
- Launch Base by Ice Face
- Icicle Falls by STJR, happyalm, InferNOr
- Midnight Abyss by STJR, InferNOr
- Sunspot Platform by AeronGreva
- Stratos Valkyrie by AeronGreva

Updated maps:
- Symmetri-Canyon
- Enabled Survival on Secluded Island

## Version 3
New maps:
- Pipeline Panic by RoyKirbs
- Jagged Creek by RoyKirbs

Enabled egg robo tag on Hydro Plant

Converted some png level select pictures to doom gfx

Implemented a temporary fix to prevent crashing on Launch Base Zone
