//make the train go choo choo when you cap the flag wow cool
local function flagSoundR(line, mo)
	if (mo.player == nil) return end
	if (mo.player.ctfteam == 1 and mo.player.gotflag == GF_BLUEFLAG and P_IsFlagAtBase(MT_REDFLAG))
		S_StartSound(nil, sfx_chuchu)
	end
end
addHook("LinedefExecute", flagSoundR, "FLAGSNDR")

local function flagSoundB(line, mo)
	if (mo.player == nil) return end
	if (mo.player.ctfteam == 2 and mo.player.gotflag == GF_REDFLAG and P_IsFlagAtBase(MT_BLUEFLAG))
		S_StartSound(nil, sfx_chuchu)
	end
end
addHook("LinedefExecute", flagSoundB, "FLAGSNDB")