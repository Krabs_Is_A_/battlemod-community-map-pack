freeslot('SPR_BIRG', 'MT_BIRDSTATUE', 'MT_BIGBIRDSTATUE', 'S_BIRDSTATUE', 'S_BIGBIRDSTATUE')
states[S_BIRDSTATUE].sprite = SPR_BIRG
states[S_BIGBIRDSTATUE] = {
	sprite = SPR_BIRG,
	frame = B
}

mobjinfo[MT_BIRDSTATUE]	= {
	//$Name "Bird Statue"
	//$Sprite BIRGA1
	//$Category "Chaos Scenery"
	doomednum = 3190,
	spawnstate = S_BIRDSTATUE,
	spawnhealth = 1000,
	reactiontime = 8,
	speed = 21*FRACUNIT,
	radius = 16*FRACUNIT,
	height = 40*FRACUNIT,
	mass = 100,
	damage = 1,
	activesound = sfx_statu2,
	flags = MF_SLIDEME|MF_SOLID|MF_PUSHABLE
}
mobjinfo[MT_BIGBIRDSTATUE]	= {
	//$Name "Big Bird Statue"
	//$Sprite BIRGB1
	//$Category "Chaos Scenery"
	doomednum = 3191,
	spawnstate = S_BIGBIRDSTATUE,
	spawnhealth = 1000,
	reactiontime = 8,
	speed = 12*FRACUNIT,
	radius = 32*FRACUNIT,
	height = 80*FRACUNIT,
	mass = 100,
	damage = 1,
	activesound = sfx_statu2,
	flags = MF_SLIDEME|MF_SOLID|MF_PUSHABLE
}
